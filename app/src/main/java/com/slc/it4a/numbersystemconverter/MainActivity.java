package com.slc.it4a.numbersystemconverter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.*;
import android.content.*;
import android.view.*;

public class MainActivity extends AppCompatActivity {
    final Context context = this;
    private EditText txtBinary,txtDecimal,txtOctal,txtHexadecimal;
    private Button btnConvert, btnClearFields;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtBinary = (EditText)findViewById(R.id.txtBinary);
        txtDecimal = (EditText)findViewById(R.id.txtDecimal);
        txtOctal = (EditText)findViewById(R.id.txtOctal);
        txtHexadecimal = (EditText)findViewById(R.id.txtHexadecimal);

        txtBinary.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                try {
                    //binary to decimal
                    int intDecimal = Integer.parseInt(txtBinary.getText().toString(), 2);
                    String strDecimal = Integer.toString(intDecimal);
                    //decimal to octal
                    String strOctal = Integer.toString(intDecimal, 8);
                    //decimal to hexadecimal
                    String strHexadecimal = Integer.toString(intDecimal, 16);

                    txtDecimal.setText(strDecimal);
                    txtOctal.setText(strOctal);
                    txtHexadecimal.setText(strHexadecimal);
                }
                catch (NumberFormatException error) {
                    launchToast("Invalid binary number");
                    clearFields();
                    return false;
                }

                return true;
            }
        });

        txtDecimal.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                try {
                    int intDecimal = Integer.parseInt(txtDecimal.getText().toString());

                    //decimal to binary
                    String strBinary = Integer.toString(intDecimal,2);
                    //decimal to octal
                    String strOctal = Integer.toString(intDecimal,8);
                    //decimal to hexadecimal
                    String strHexadecimal = Integer.toString(intDecimal,16);

                    txtBinary.setText(strBinary);
                    txtOctal.setText(strOctal);
                    txtHexadecimal.setText(strHexadecimal);
                }
                catch (NumberFormatException error) {
                    launchToast("Invalid decimal number");
                    clearFields();
                    return false;
                }

                return true;
            }
        });

        txtOctal.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                try {
                    int intOctal = Integer.parseInt(txtOctal.getText().toString(),8);

                    //octal to binary
                    String strBinary = Integer.toString(intOctal,2);
                    //octal to decimal
                    String strDecimal = Integer.toString(intOctal,10);
                    //octal to hexadecimal
                    String strHexadecimal = Integer.toString(intOctal,16);

                    txtBinary.setText(strBinary);
                    txtDecimal.setText(strDecimal);
                    txtHexadecimal.setText(strHexadecimal);
                }
                catch (NumberFormatException error) {
                    launchToast("Invalid octal number");
                    clearFields();
                    return false;
                }

                return true;
            }
        });

        txtHexadecimal.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                try {
                    int intHexadecimal =  Integer.parseInt(txtHexadecimal.getText().toString(),16);

                    //hexadecimal to binary
                    String strBinary = Integer.toString(intHexadecimal,2);
                    //hexadecimal to decimal
                    String strDecimal = Integer.toString(intHexadecimal,10);
                    //hexadecimal to octal
                    String strOctal = Integer.toString(intHexadecimal,8);

                    txtBinary.setText(strBinary);
                    txtDecimal.setText(strDecimal);
                    txtOctal.setText(strOctal);
                }
                catch (NumberFormatException error) {
                    launchToast("Invalid hexadecimal number");
                    clearFields();
                    return false;
                }

                return true;
            }
        });

        btnClearFields = (Button)findViewById(R.id.btnClearFields);
        btnClearFields.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearFields();
                launchToast("Fields cleared");
            }
        });
    }

    private void clearFields() {
        txtBinary = (EditText)findViewById(R.id.txtBinary);
        txtDecimal = (EditText)findViewById(R.id.txtDecimal);
        txtOctal = (EditText)findViewById(R.id.txtOctal);
        txtHexadecimal = (EditText)findViewById(R.id.txtHexadecimal);

        txtBinary.setText("");
        txtDecimal.setText("");
        txtOctal.setText("");
        txtHexadecimal.setText("");
    }

    private void launchToast(String strMessage) {
        Toast.makeText(context,strMessage, Toast.LENGTH_SHORT).show();
    }
}
